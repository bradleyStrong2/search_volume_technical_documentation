
\documentclass[12pt]{article}%
\usepackage{amsfonts}
\usepackage{fancyhdr}
\usepackage{comment}
\usepackage{url}
\usepackage[a4paper, top=2.5cm, bottom=2.5cm, left=2.2cm, right=2.2cm]%
{geometry}
\usepackage{times}
\usepackage{amsmath}
\usepackage{changepage}
\usepackage{amssymb}
\usepackage{graphicx}%
\setcounter{MaxMatrixCols}{30}
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\usepackage{amsmath}
\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf}



\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Z}{\mathbb{Z}}

\begin{document}
\SweaveOpts{concordance=TRUE}

\title{Documentation for Search volume Forecasting and the Adjustment Procedure}
\author{AdGooroo}
\date{\today}
\maketitle
\section{Search Volume Adjustment}


First we will discuss a general formula for how the Jumpshot's search volume is adjusted. Essentially we use the most recently
report Google volume for that keyword target and use that as an adjustment to map the Jumpshot's forecasted volume 
to what we would expect Google to report.
This is the search volume used in all of the applications calculations.Regarding notation
we will use the hat symbol (such as $\hat{X}$) above a variable to indicate it is a predicted value versus an actual.

 Lets G be defined as search volume from Google, let JS be defined as search volume from jumpshot and
 let our final search volume used in the user interface calculations to be defined as SV. 
 Also, let us denote $X_{i,t}$  any of the search volume components for the ith target and any time t,
 where $ X \in (G,SV,JS)$. Define $t = t_{baseline}$ as the last reported Google 
 search volume and $t = t_{forecast}$ as forecast time for the $ith$ target. (Note, $t = t_{forecast}$
 is really just T+1 where T is the current time period). Then our search volume
 forcast for  $t = t_{forecast}$ any $ith$ target (aggregated across platform) is:
 
  \begin{equation}
     SV_{i,t_{forecast}} =  G_{i,t_{baseline}}\cdot\frac{\hat{J_{i,t_{forecast}}}}{J_{i,t_{baseline}}}
  \end{equation}
  
  Note, here the $\frac{\hat{J_{i,t_{forecast}}}}{J_{i,t_{baseline}}}$ piece is an adjustment factor from the last captured
  actual Google Search Volume. Also, to be clear, $SV_{i,t_{forecast}}$ is just really a forecast of what 
  google will report for that $ith$ target at $t = t_{forecast}$. That is,  $ SV_{i,t_{forecast}} = \hat{G_{i,t_{forecast}}}$
  
  \subsection{If Googles Baseline Value is missing}
  On Rare cases we may not have a Google Baseline volume for a particular target.
  If, $G_{i,t_{baseline}}$ is missing then we use, 
  \begin{equation}
  SV_{i,t_{forecast}} = 101 \cdot J_{i,t_{forecast}}
  \end{equation}
  Note, the constant 101 was found by regressing $G_{i,t}$ as a function of $J_{i,t}$ on historical data
  and setting the intercept to $0$.
  
  
  \subsection{Allocating Search Volume to Platform}
  Let $X_{i,t}^{P}$ be any search volume source $ X \in (G,SV,JS)$ and $p$ denote platforms in $p \in (d,m)$
  where d is desktop and m is mobile.Then for our $ith$ target at any time $t$ for the PC platform the estimate of search volume is: 
  \begin{equation}
  SV_{i,t_{forecast}} =  G_{i,t_{baseline}}^{p \in (D,M)}\cdot\frac{\hat{J_{i,t_{forecast}}^{d}} +  \hat{J_{i,t_{forecast}}^{m}   }}{J_{i,t_{baseline}}}
  \end{equation}
  Then to allocate this volume to each platform we compute an estimate of the platform allocation factor $\theta$ at time $t$ for 
  an arbitrary keyword target $i$, for each $p \in (d,m)$ by: 
  \begin{equation}
  \hat{\theta_{i,t_{forecast}}^{p}} =  \frac{\hat{J_{i,t_{forecast}}^{p}}} {\hat{J_{i,t_{forecast}}^{d}} +  \hat{J_{i,t_{forecast}}^{m}} } 
  \end{equation}
  So our final estimate for desktop search volume is then: 
  \begin{equation}
  SV_{i,t_{forecast}}^{d} =  \hat{\theta_{i,t_{forecast}}^{d}}\cdot\hat{SV_{i,t_{forecast}}} 
  \end{equation}
  Similiarly for the estimated mobile search volume we have: 
  \begin{equation}
  SV_{i,t_{forecast}}^{m} =  \hat{\theta_{i,t_{forecast}}^{m}}\cdot\hat{SV_{i,t_{forecast}}} 
  \end{equation}



 \subsubsection{Restricting the Adjustment Values}
  Jumpshot is panel data and therefore a subset of the overall population of Google users. The search volume 
  history for a particular keyword in Jumpshot is subject to noise and flucations not observed in Google. What this means is    that, in  general, the adjustment factor computed from Jumpshots panel data will sometimes be substantially higher or lower than what is seen in happening in Google. Note, on average, the noise will diminsh as the search volume gets larger; so this is more of an issue when dealing with long-tail keywords. 
  To deal with this we have created the following range restrictions on the adjustment by Google's baseline captured volume. Note,that
  this range is applied to $  SV_{i,t_{forecast}}$ prior to allocating to platform.
 \begin{table}[h!]
\centering
 \begin{tabular}{||c c||} 
 \hline
 Google's Baseline Volume & Restriction \\ [1ex] 
 \hline\hline
 > 1 Million & [0.60,1.40]  \\ 
 (100,000:1 Million]  & [0.65,1.35]   \\
 (10,000:100,000] & [0.70,1.30]   \\
 (1000:10,000] & [0.75,1.25]  \\
 < 1000 & [0.80,1.20] \\ [1ex] 
  
 \hline
 \end{tabular}
\end{table}

\section{Search Volume Forecasting Model}
  For each keyword target and platform combination we first determine whether there or not there is a sufficient 
  number of recent series points. This is tested by counting the number of datapoints for a particular target 
  for the most recent year. If there is at least one search for 11 out of 12 months it is deemed sufficient for use in a formal  statistical model, otherwise not. Here what insufficient data implies is that the keyword is so infrequently searched 
  that it is completely not present from historical Jumpshot data in the past year for one or more months.
  
 \subsection{Sufficient Data Forecasting}
 For each target and platform we first fit both ARIMA and ETS models, holding out the most recent 3 datapoints. 
 The component structure and parameters estimates are automatically determine/fit and then forecasts are generated for the    datapoints and are compared against the actuals. The better fitting approach, measured using the Log of RMSE, is then used 
 and fit on the entire series. The component structure is then re-determined and the parameter estimates fit. The forecast
 is then generated. One should note that some ARIMA Models have an equivalent ETS version.
 \subsubsection{Notes on ARIMA}
  Seasonal ARIMA stands for Seasonal \textbf{A}uto \textbf{R}egressive \textbf{I}ntegrated \textbf{M}oving 
  \textbf{A}verage Model.
  The auto.arima procedure fits a seasonal ARIMA model. A general form for this model is:
    \begin{gather}
    ARIMA(p,d,q)\cdot(P,D,Q)S
    \end{gather}
    Where $p$ is the non-seasonal Autoregressive order(AR), $d$ is the non-seasonal differencing, $q$ is the non seasonal          Moving Average(MA) order, $P$ is the Seasonal differencing, $Q$ seasonal MA order and S is the time span of the
    repetition of the seasonal structure.

  
 \subsubsection{Notes on ETS}
 ETS stands for \textbf{E}xponen\textbf{T}ial \textbf{S}moothing
  The ETS procedure fits an exponental model. A general form for this model is:
    \begin{gather}
    ETS(Error,Trend,Seaonal)
    \end{gather}
 \subsection{Insufficient Data Forecasting}
 For the insufficient data portion we use a weighted mean approach. Here the more recent time points are given more weight.     Let $J_{i,T}$ be the jumpshot search volume for the $ith$ target at current 
 time $T$ for platform $p \in (d,m)$. Note that time $t=1$ reprents the earliest time point. Then we set the forecast
 for Jumpshot for the $ith$ platform at time $T+1$ to be:
 
   \begin{gather}
     \hat{J_{i,T + 1}^{P}} =  \sum_{t=1}^{T} W_{t}\cdot J_{i,t} 
     \quad\ \quad 
      \forall i
      \quad\text{and}\quad 
      \nonumber  p \in (d,m)
    \end{gather}
    Where $wi$ are determined by,
   \begin{gather}
    \nonumber W_{t=1} =  c \cdot 1 \\
   \nonumber W_{t=2} =  c \cdot 2 \\
   \nonumber . \\
    W_{t=T} =  c \cdot T
    \end{gather}
    The scalar $c$ can be found by,
     \begin{gather}
      c = \frac{1}{1 + 2 +...+ T}  \quad\ \quad\text{So that,}\quad \sum_{t=1}^{T} W_{t} =1
     \end{gather}
   
    
    


\end{document}
